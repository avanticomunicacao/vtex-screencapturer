# VTEX ScreenCapturer

O projeto VTEX ScreenCapturer é uma ferramenta de apoio para equipe técnica da Avanti e tem como principal objetivo capturar telas de e-commerces baseados na plataforma VTEX de forma automatizada.

Através de arquivos de configuração é possível simular o acesso às páginas dos e-commerces com variadas configurações de resolução, browsers, sistemas operacionais e etc.

## Instalação

O VTEX ScreenCapturer foi desenvolvido em Node.js. Por isso é necessário tê-lo instalado. Caso você use Mac provavelmente já o tem instalado, caso contrário pode efetuar o download em [https://nodejs.org/en/download/](https://nodejs.org/en/download/).

Para conferir se o Node.js já está instalado você poderá acessar o terminal de linha de comando e executar o comando abaixo:

```
node -v
```

Caso o Node.js esteja corretamente instalado aparecerá em seu console algo similar a isto:

```
v4.4.4
```

Além do Node.js você precisará do Git instalado na sua máquina. Caso você use Mac provavelmente já o tem instalado, caso contrário poderá efetuar o download em [https://git-scm.com/downloads](https://git-scm.com/downloads).

Para conferir se o Git já está instalado você poderá acessar o terminal de linha de comando e executar o comando abaixo:

```
git --version
```

Caso o Git esteja corretamente instalado aparecerá em seu console algo similar a isto:

```
git version 2.6.2
```

Após confirmar que o Node está instalado e funcionando deverá ser feito o clone dos projeto no BitBucket. Para isso você deverá ir no diretório onde deseja instalar o VTEX ScreenCapturer e executar o seguinte comando:

```
git clone https://<seu-usuario>@bitbucket.org/avanticomunicacao/vtex-screencapturer.git
```

> IMPORTANTE: O seu usuário deve fazer parte do time da Avanti no BitBucket

Será criado o diretório vtex-screencapturer que contém o VTEX ScreenCapturer propriamente dito. Você deverá acessá-lo através do seguinte comando:

```
cd vtex-screencapturer
```

O último passo para a instalação do VTEX ScreenCapturer é instalar as dependências do Node.js através do comando abaixo:

```
npm install
```

Caso você tenha completado todas essas etapas e não tenha encontrado erros o VTEX ScreenCapturer está pronto para o uso. 

## Uso

Para a utilização VTEX ScreenCapturer alguns conceitos são importantes.

* **Contextos**: definem as configurações de browsers, resoluções, sistemas operacionais e etc. para a captura das telas. As opções são as mesmas usadas para o PhantonJS na abstração node-webshot e podem ser encontradas em [https://github.com/brenden/node-webshot#options](https://github.com/brenden/node-webshot#options). Uma lista de user agents podem ser encontradas em [http://www.useragentstring.com/pages/useragentstring.php](http://www.useragentstring.com/pages/useragentstring.php).
* **Url's**: são links para as páginas que você deseja capturar telas.

A combinação de todos os contextos com todas as url's irá definir a quantidade de telas geradas.
Para cada site deverá ser criado um arquivo de configuração dentro da pasta site definindo os contextos e url's. Abaixo segue um arquivo de exemplo:

```
(function(){

	exports.siteConfig = {

		contexts: [
			{
				name: 'win_ie9_1280_800',
				screenSize: {
					width: 1280,
					height: 800
				},
				shotSize: {
					width: 1280,
					height: 'all'
				},
				userAgent: 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 7.1; Trident/5.0)'
			},
			{
				name: 'mac_chrome_1024_768',
				screenSize: {
					width: 1024,
					height: 768
				},
				shotSize: {
				    width: 1024,
					height: 'all'
				},
				userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36'
			}
		],

		urls: [
			{
				filename: 'home',
				url: 'http://www.labellamafia.com.br/'
			},
			{
				filename: 'categoria_leggins',
				url: 'http://www.labellamafia.com.br/leggings'
			},
			{
				filename: 'legging_blue_forms',
				url: 'http://www.labellamafia.com.br/legging-blue-forms-fcl11207-02/p'
			},
			{
				filename: 'jaqueta_hustle',
				url: 'http://www.labellamafia.com.br/jaqueta-hustle-mjq10218-01/p'
			}
		]
	}

})();
```

Para adicionar novos sites e deixá-los disponíveis para a captura de telas basta criar um novo arquivo dentro do diretório site da aplicação.

Após ter configurados todos os sites com seus respectivos contextos e url's você poderá executar o VTEX ScreenCapturer propriamente dito. Utilizando o comando abaixo você listará todos sites disponíves captura de tela:

```
node capture
```

Este comando deverá produzir uma saída similar a esta abaixo:

```
Informe um dos sites listados abaixo (node capture <site>):

drogariasaopaulo
fibracirurgica
labellamafia
```

Para iniciar a caputura de teles de um determinado site basta usar um comando similar a este abaixo:

```
node capture labellamafia
```

Isso irá produzir uma saída similar a esta abaixo:

```
Serão geradas 6 imagens de telas (3 páginas em 2 contextos). Aguarde...

1 telas capturadas. Falta capturadas 5 telas
2 telas capturadas. Falta capturadas 4 telas
3 telas capturadas. Falta capturadas 3 telas
4 telas capturadas. Falta capturadas 2 telas
5 telas capturadas. Falta capturadas 1 telas
6 telas capturadas. Falta capturadas 0 telas

Captura finalizada. 6 telas capturadas. Abrindo screens/labellamafia/1469730201982/.
```

> IMPORTANTE: A captura de telas depende de vários fatores externos, como velocidade do site, velocidade de conexão com a internet, etc. Portanto, o tempo de captura pode variar e demorar alguns minutos. 
>
> Se você estiver usando Mac, após todas as telas terem sido capturadas uma janela do Finder será aberta no diretório onde as imagens foram geradas.

### Diretórios de saída
Todas as capturas de telas são geradas dentro diretório screens. Para cada site será um criado um diretório como o nome dele. Para cada execução do programa será criado um diretório tendo como nome um timestamp. Dentro dele será gerado um diretório para cada nome de contexto. Por fim, serão criadas as imagens com os nomes configurados nas Url's. Abaixo um exemplo de como deve ficar essa estrutura:

* screens
	* labellamafia
		* 1469720258735
			* mac\_chrome\_1024\_768
			* win\_ie9\_1280\_800
		* 1469720363873
			* mac\_chrome\_1024\_768
			* win\_ie9\_1280\_800
	* drogariasaopaulo
		* 1469720259874
			* mac\_chrome\_1024\_768
			* win\_ie9\_1280\_800
		* 1469720367749
			* mac\_chrome\_1024\_768
			* win\_ie9\_1280\_800

## Atualização

Para atualizar a versão do VTEX ScreenCapturer basta seguir o padrão do Git utilizando o comando abaixo:

```
git pull
```
Caso você realize alguma alteração nas configurações dos arquivos dos sites também poderá fazer commit das alterações usando a sequência de comandos:

```
git add --all
git commit -m "Especifique aqui as alterações realizadas."
git push
```
Considerando que este projeto deve ser usado pela equipe técnica da Avanti acreditamos que utilização dos comandos acima não será um problema. De qualquer forma, se encontrar alguma dificuldade em executá-los, envie um e-mail para [dev@penseavanti.com.br](mailto:dev@penseavanti.com.br) que alguma alma caridosa fará questão de ajudá-lo.