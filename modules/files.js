var fs = require('fs');

exports.listSites = function() {
	return fs.readdirSync('sites');
};

exports.siteExists = function(site) {
	try {
		fs.accessSync('sites/' + site + '.js', fs.F_OK);
		return true;
	} catch (e) {
		return false;
	}
}