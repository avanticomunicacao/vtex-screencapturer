var webshot = require('webshot'),
	childProcess = require('child_process');

exports.capture = function(site) {

	var siteConfig = require('../sites/' + site + '.js').siteConfig;

	var totalUrls = siteConfig.urls.length;
	var totalContexts = siteConfig.contexts.length;
	var totalPrints = totalContexts * totalUrls;

	console.log('');
	console.log('Serão geradas ' + totalPrints + ' imagens de telas (' + totalUrls + ' páginas em ' + totalContexts + ' contextos). Aguarde... ');

	var captureds = 0
	var destinationDir = 'screens/' + site + '/' + new Date().getTime() + '/';

	for (var i = 0; i < totalContexts; i++) {
		var context = siteConfig.contexts[i];

		for (var j = 0; j < totalUrls; j++) {
			var url = siteConfig.urls[j];
			var destinationFile = destinationDir + context.name + '/' + url.filename + '.png';

			webshot(url.url, destinationFile, context, function(err) {
				captureds++;

				console.log(captureds + ' telas capturadas. Falta capturadas ' + (totalPrints - captureds) + ' telas');
				if (captureds == totalPrints && process.platform == 'darwin') {
					console.log('');
					console.log('Captura finalizada. ' + totalPrints + ' telas capturadas. Abrindo ' + destinationDir + '.');
					console.log('');
					childProcess.exec('open ./' + destinationDir);
				}
			});

		}
	}
	console.log('');

}