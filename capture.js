var files = require('./modules/files.js');

var site = process.argv[2];

if (site) {
	if (!files.siteExists(site)) {
		console.log('');
		console.log('Site "' + site + '" não encontrado');
		console.log('');
	} else {
		var capturer = require('./modules/capturer.js');
		capturer.capture(site);
	}
} else {
	var sites = files.listSites();
	console.log('');
	console.log('Informe um dos sites listados abaixo (node capture <site>):');
	console.log('');
	for (var i = 0; i < sites.length; i++) {
		console.log(sites[i].replace('.js', ''));
	}
	console.log('');
}

