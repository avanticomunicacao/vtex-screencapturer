(function(){

	exports.siteConfig = {

		contexts: [
			{
				name: 'win_ie9_1280_800',
				screenSize: {
					width: 1280,
					height: 800
				},
				shotSize: {
					width: 1280,
					height: 'all'
				},
				userAgent: 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 7.1; Trident/5.0)'
			},
			{
				name: 'mac_chrome_1024_768',
				screenSize: {
					width: 1024,
					height: 768
				},
				shotSize: {
				    width: 1024,
					height: 'all'
				},
				userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36'
			}
		],

		urls: [
			{
				filename: 'home',
				url: 'http://www.labellamafia.com.br/'
			},
			{
				filename: 'categoria_leggins',
				url: 'http://www.labellamafia.com.br/leggings'
			},
			{
				filename: 'legging_blue_forms',
				url: 'http://www.labellamafia.com.br/legging-blue-forms-fcl11207-02/p'
			},
			{
				filename: 'jaqueta_hustle',
				url: 'http://www.labellamafia.com.br/jaqueta-hustle-mjq10218-01/p'
			}
		]
	}

})();