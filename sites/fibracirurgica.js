(function(){

	exports.siteConfig = {

		contexts: [
			{
				name: 'mac_chrome_1024_768',
				screenSize: {
					width: 1024,
					height: 768
				},
				shotSize: {
				    width: 1024,
					height: 'all'
				},
				userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36'
			},
			{
				name: 'iphone_safari',
				screenSize: {
					width: 360,
					height: 640
				},
				shotSize: {
				    width: 360,
					height: 'all'
				},
				userAgent: 'Mozilla/5.0 (Linux; U; Android 4.0.3; ko-kr; LG-L160L Build/IML74K) AppleWebkit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30'
			}
		],

		urls: [
			{
				filename: 'home',
				url: 'http://www.fibracirurgica.com.br'
			}
		]
	}

})();